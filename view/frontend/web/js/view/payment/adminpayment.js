define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'adminpayment',
                component: 'Kowal_AdminPayment/js/view/payment/method-renderer/adminpayment-method'
            }
        );
        return Component.extend({});
    }
);