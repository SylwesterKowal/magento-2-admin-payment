<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\AdminPayment\Observer\Payment;
use Magento\Framework\App\State;
use Magento\Framework\App\Area;

class MethodIsActive implements \Magento\Framework\Event\ObserverInterface
{

    public function __construct(State $state)
    {
        $this->state       = $state;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $result          = $observer->getEvent()->getResult();
        $method_instance = $observer->getEvent()->getMethodInstance();
        $quote           = $observer->getEvent()->getQuote();


        /* Enable All payment gateway  exclude Your payment Gateway*/
        if ($method_instance->getCode() == 'adminpayment') {
            if($this->isAdmin()){
                $result->setData('is_available', true);
            }else{
                $result->setData('is_available', false);
            }

        }
        else
        {
            $result->setData('is_available', true);
        }
    }

    public function isAdmin(): bool
    {
        return $this->state->getAreaCode() === Area::AREA_ADMINHTML;
    }
}

